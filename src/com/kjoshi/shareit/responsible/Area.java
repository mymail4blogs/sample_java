/**
 * 
 */
package com.kjoshi.shareit.responsible;

import com.kjoshi.shareit.responsible.enums.ShapeType;

/**
 * @author kailash
 * 
 *         The main intention in Chain Of Responsibility is to decouple the
 *         origin of the request and handling of the request such that the
 *         origin of the request need not worry who and how its request is being
 *         handled as long as it gets the expected outcome. By decoupling the
 *         origin of the request and the request handler we make sure that both
 *         can change easily and new request handlers can be added without the
 *         origin of the request i.e client being aware of the changes.
 * 
 *         In this pattern we create a chain of objects such that each object
 *         will have a reference to another object which we call it as successor
 *         and these objects are responsible for handling the request from the
 *         client. All the objects which are part of the chain are created from
 *         classes which confirm to a common interface there by the client only
 *         needs to be aware of the interface and not necessarily the types of
 *         its implementations. The client assigns the request to first object
 *         part of the chain and the chain is created in such a way that there
 *         would be at least one object which can handle the request or the
 *         client would be made aware of the fact that its request couldn�t be
 *         handled.
 */
public class Area {
	private Area successor;

	public void findArea(String label) {
		if (getSuccessor() != null) {
			getSuccessor().findArea(label);
		} else {
			System.out.println("Not able to find Area for : " + label);
		}
	}

	protected boolean canHandleShape(ShapeType shapeType, String label) {
		return (shapeType.label.equals(label));

	}

	public Area getSuccessor() {
		return successor;
	}

	public void setSuccessor(Area successor) {
		this.successor = successor;
	}

}
