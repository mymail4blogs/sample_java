/**
 * 
 */
package com.kjoshi.shareit.responsible;

import com.kjoshi.shareit.responsible.enums.ShapeType;

/**
 * @author kailash
 *
 */
public class Rectangle extends Area {
	public Rectangle(Area successor) {
		this.setSuccessor(successor);
	}

	public void findArea(String label) {
		if (canHandleShape(ShapeType.RECTANGLE, label)) {
			System.out.println("Area calculated for  : "
					+ ShapeType.RECTANGLE.label);
		} else {
			super.findArea(label);
		}
	}
}
