/**
 * 
 */
package com.kjoshi.shareit.responsible.execute;

import java.util.ArrayList;
import java.util.List;

import com.kjoshi.shareit.responsible.Area;
import com.kjoshi.shareit.responsible.Circle;
import com.kjoshi.shareit.responsible.Rectangle;
import com.kjoshi.shareit.responsible.Square;
import com.kjoshi.shareit.responsible.Triangle;
import com.kjoshi.shareit.responsible.enums.ShapeType;

/**
 * @author kailash
 * 
 */
public class ChainOfResponsibility {
	public static void main(String[] args) {
		// I am keeping Circle in the last in the chain so starting from Circle
		Area circle = new Circle();
		Area triangle = new Triangle(circle);
		Area rectangle = new Rectangle(triangle);
		Area square = new Square(rectangle);
		for (String shape : listOfShapes()) {
			square.findArea(shape);
		}
	}

	private static List<String> listOfShapes() {
		List<String> list = new ArrayList<String>();
		for (ShapeType shapeType : ShapeType.values()) {
			list.add(shapeType.label);
		}
		// added this string for a message that not able to find area
		list.add("rhombus");
		return list;
	}

}
