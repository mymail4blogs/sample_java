/**
 * 
 */
package com.kjoshi.shareit.responsible;

import com.kjoshi.shareit.responsible.enums.ShapeType;

/**
 * @author kailash
 * 
 */
public class Square extends Area {
	public Square(Area successor) {
		this.setSuccessor(successor);
	}

	public void findArea(String label) {
		if (canHandleShape(ShapeType.SQUARE, label)) {
			System.out.println("Area calculated for  : "
					+ ShapeType.SQUARE.label);
		} else {
			super.findArea(label);
		}
	}
}
