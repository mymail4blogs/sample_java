/**
 * 
 */
package com.kjoshi.shareit.responsible;

import com.kjoshi.shareit.responsible.enums.ShapeType;

/**
 * @author kailash
 *
 */
public class Triangle extends Area {
	public Triangle(Area successor) {
		this.setSuccessor(successor);
	}

	public void findArea(String label) {
		if (canHandleShape(ShapeType.TRIANGLE, label)) {
			System.out.println("Area calculated for  : "
					+ ShapeType.TRIANGLE.label);
		} else {
			super.findArea(label);
		}
	}
}
