/**
 * 
 */
package com.kjoshi.shareit.responsible;

import com.kjoshi.shareit.responsible.enums.ShapeType;

/**
 * @author kailash
 *
 */
public class Circle extends Area {
	

	public void findArea(String label) {
		if (canHandleShape(ShapeType.CICRLE, label)) {
			System.out.println("Area calculated for  : "
					+ ShapeType.CICRLE.label);
		} else {
			super.findArea(label);
		}
	}
}
